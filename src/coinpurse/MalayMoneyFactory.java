package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Factory class that use for create malay money both coin and banknote.
 * This is the concrete class of MoneyFactory.
 * @author Pongsakorn Somsri
 *
 */
public class MalayMoneyFactory extends MoneyFactory{

	public MalayMoneyFactory() {
		super();
	}
	/**A Malay coin currency use for check the value of the input value*/
	private static final List<Double> malayCoin = Arrays.asList(0.05,0.10,0.20,0.50);
	/**A Malay banknote currency use for check the value of the input value*/
	private static final List<Double> malayBankNote = Arrays.asList(1.0,2.0,5.0,10.0,20.0,100.0);
	/**Malay coin currency name*/
	private static final String coinCurrency = "Sen";
	/**Malay bank note currency name*/
	private static final String bankNoteCurrency = "Ringgit";

	@Override
	public Valuable createMoney(double value) {
		if(malayCoin.contains(value))
			return new Coin(value*100,coinCurrency);
		
		if(malayBankNote.contains(value))
			return new BankNote(value,bankNoteCurrency);
		
		throw new IllegalArgumentException();
	}






}
