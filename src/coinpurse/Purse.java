package coinpurse;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;



/**
 * A Valuable purse contains Valuables. You can insert Valuables, withdraw moneyList, check the
 * balance, and check if the purse is full. When you withdraw moneyList, the Valuable
 * purse decides which Valuables to remove.
 * 
 * @author Pongsakorn Somsri
 */
public class Purse {
	/** Collection of Valuables in the purse. */
	private List<Valuable> moneyList;
	/**
	 * Capacity is maximum NUMBER of Valuables the purse can hold. Capacity is set
	 * when the purse is created.
	 */
	private int capacity;
	
	private final ValueComparator comparator = new ValueComparator();


	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity
	 *            is maximum number of Valuables you can put in purse.
	 */
	public Purse(int capacity) {
		this.capacity = capacity;
		moneyList = new ArrayList<Valuable>();
	}

	/**
	 * Count and return the number of Valuables in the purse. This is the number of
	 * Valuables, not their value.
	 * 
	 * @return the number of Valuables in the purse
	 */
	public int count() {
		return moneyList.size();
	}

	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	public int getBalance() {
		double balance = 0;

		for (int i = 0; i < this.count(); i++)
			balance += moneyList.get(i).getValue();

		return (int)balance;
	}

	/**
	 * Return the capacity of the Valuable purse.
	 * 
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 */
	public boolean isFull() {
		boolean check = false;
		if (this.getCapacity() == this.count())
			check = true;
		return check;
	}

	/**
	 * Insert a Valuable into the purse. The Valuable is only inserted if the purse has
	 * space for it and the Valuable has positive value. No worthless Valuables!
	 * 
	 * @param newValuable
	 *            is a Valuable object to insert into purse
	 * @return true if Valuable inserted, false if can't insert
	 */
	public boolean insert(Valuable newValuable) {
		// if the purse is already full then can't insert anything.
		boolean check = false;
		if (isFull() == false && newValuable.getValue()!=0) {
			check = true;
			moneyList.add(newValuable);
		}
		return check;
	}

	/**
	 * Withdraw the requested amount of moneyList. Return an array of Valuables
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * @param amount
	 *            is the amount to withdraw
	 * @return array of Valuable objects for moneyList withdrawn, or null if cannot
	 *         withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {
		
		if (amount <= 0)
			return null;

		List<Valuable> temp = new ArrayList<Valuable>();
		
		Collections.sort(moneyList,comparator);
		Collections.reverse(moneyList);

		double balanceTemp = 0;
		for (int i = 0; i < this.count(); i++) {
			Valuable thisValuable = moneyList.get(i);
			if (balanceTemp + thisValuable.getValue() <= amount) {
				temp.add(thisValuable);
				balanceTemp += thisValuable.getValue();
			}
		}

		if (balanceTemp == amount) {
			// success
			Valuable[] valuablesWithdraw = new Valuable[temp.size()];
			// copy temp into the array
			temp.toArray(valuablesWithdraw);

			// for each Valuable in the temp list, remove a matching Valuable
			// from the Valuables in the purse
			for(int i=0;i<temp.size();i++)
				moneyList.remove(temp.get(i));
			
			return valuablesWithdraw;
//			
		}

		
		return null;
	}

	/**
	 * toString returns a string description of the purse contents. It can
	 * return whatever is a useful description..
	 * @return String of what is in purse
	 */
	public String toString() {
		String toString = "";
		for (int i = 0; i < this.count(); i++) {
			toString += moneyList.get(i).toString() + "\n";
		}
		return toString;
	}

}

