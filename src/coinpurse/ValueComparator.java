package coinpurse;

import java.util.Comparator;

/**
 * Class that use for compare 2 object that implement valuable.
 * @author Pongsakorn Somsri
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	
	/**
	 * Method that use to compare 2 value.
	 * @param value1 First Object that want to compare
	 * @param value2 Second Object that want to compare 
	 * @return -1 if first object is less than second one,0 if equal,1 if first one is higher
	 */
	public int compare(Valuable value1, Valuable value2) {
		if(value1.getValue()>value2.getValue())
			return 1;
		if(value1.getValue()<value2.getValue())
			return -1;
		return 0;
	}
	

}
