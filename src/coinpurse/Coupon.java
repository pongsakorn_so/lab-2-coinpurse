package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * A coupon that contain a color red,blue or green.Value of color is following 100,50,20. 
 * @author Pongsakorn Somsri
 *
 */

public class Coupon extends AbstractValuable{

	private String color;
	private Map<String,Double> valueOfColor = new HashMap<String,Double>();

	/**
	 * Coupon that has color if color that enter isn't red blue or green color will be null.
	 * @param color is use for set color and value of this object
	 * 
	 */
	public Coupon(String color){	
		valueOfColor.put("red", 100.0);
		valueOfColor.put("blue", 50.0);
		valueOfColor.put("green", 20.0);

		if(valueOfColor.containsKey(color.toLowerCase())){
			this.color = color;
			this.value = valueOfColor.get(color.toLowerCase());

		}
		else{
			this.color = null;
			this.value = 0;
		}

	}

	/**
	 * A method that use to get a color of coupon in String type.
	 * @return String of Coupon following this format "Color Coupon"
	 */
	public String toString(){
		return String.format("%s Coupon",color);
	}



}
