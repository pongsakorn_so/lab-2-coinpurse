package coinpurse;

/**
 * A Banknote class that has value of 100,50 or 20.
 * @author Pongsakorn Somsri
 *
 */
public class BankNote extends AbstractValuable{
	private static int nextSerialNumber = 1000000 ;
	private int serialNumber;
	private String currency;
	//private double value;

/**
 * Banknote that create by give value into it.Can only has 100,50,20 note.
 * @param value of banknote 
 */
	public BankNote(double value,String currency){
		super();
		
			this.value = value;
			this.currency = currency;
			serialNumber = getNextSerialNumber();	

	}

	/**
	 * Get serial number and add the next serial number by one.
	 * @return nextserialnumber
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber++;
	}
		
	
	/**
	 * method that get value of banknote and serial number.
	 * @return String of banknote
	 */
	public String toString(){
		return String.format("%.0f-%s Banknote", value,this.currency);
	}


}
