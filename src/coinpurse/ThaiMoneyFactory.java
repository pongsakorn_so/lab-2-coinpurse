package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Factory class that use for create Thai money both coin and banknote.
 * This is the concrete class of MoneyFactory.
 * @author Pongsakorn Somsri
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{

	public ThaiMoneyFactory() {
		super();
	}

	/**A Thai coin currency use for check the value of the input value*/
	private static final List<Double> ThaiCoin = Arrays.asList(1.0,2.0,5.0,10.0);
	/**A Thai banknote currency use for check the value of the input value*/
	private static final List<Double> ThaiBankNote = Arrays.asList(20.0,50.0,100.0,500.0,1000.0);
	/**Thai currency name*/
	private static final String currency = "Baht";

	@Override
	public Valuable createMoney(double value) {
		if(ThaiCoin.contains(value))
			return new Coin(value,this.currency);
		
		if(ThaiBankNote.contains(value))
			return new BankNote(value,this.currency);
		
		throw new IllegalArgumentException();
	}






}
