package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Pongsakorn Somsri
 */
public class Coin extends AbstractValuable{

	private String currency;
	
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value , String currency) {
		this.value = value;
		this.currency = currency;
	}
	
	/** 
	 * Create String of coin.
	 * @return return value of coin in string
	 */
	public String toString(){
		return String.format("%.0f-%s coin",value,this.currency);
	}

}

