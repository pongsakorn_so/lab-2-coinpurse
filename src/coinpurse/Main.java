package coinpurse;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Pongsakorn Somsri
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {

        // 1. create a Purse
    	//Purse purse1 = new Purse(10);

        // 2. create a ConsoleDialog with a reference to the Purse object
    	//ConsoleDialog consoleDialog1 = new ConsoleDialog(purse1);

        // 3. run() the ConsoleDialog
    	//consoleDialog1.run();
    	
    	MoneyFactory factory = MoneyFactory.getInstance();
    	Valuable m = factory.createMoney( 5 );
    	System.out.println(m.toString());
    	Valuable m2 = factory.createMoney("1000.0");
    	System.out.println(m2.toString());
    	Valuable m3 = factory.createMoney(0.05);




    }
}
