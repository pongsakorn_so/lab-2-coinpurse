package coinpurse;

/**
 * Superclass of.
 * @author Pongsakorn Somsri
 *
 */
public abstract class AbstractValuable implements Valuable{
	
	protected double value;
	
	/**
	 * Create a AbstractValuable object.
	 */
	public AbstractValuable(){
		//this.value = this.getValue();
	}
	
	/** 
	 * Compare the value of coin to other coin.
	 * @param other is the coin that want to compare to
	 * @return -1 if less than coin2,0 if equal,1 if higher
	 */
	public int compareTo(Valuable other){
		if(this.getValue()>other.getValue())
			return 1;
		else if(this.getValue()<other.getValue())
			return -1;
		
		return 0;
		
	}
	
	/** 
	 * Method that check whether value of coin is equals or not.
	 * @param obj that want to test
	 * @return true if value is equals.false if not equals
	 */
	public boolean equals(Object obj){
		if(obj.getClass() == null)
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;
					
		Valuable other = (Valuable)obj;
		
		if(this.getValue()==other.getValue())
			return true;
		
		return false;
	}
	
	/**
	 * method that get value of this object.
	 * @return value
	 */
	public double getValue(){
		return value;
	}
}
