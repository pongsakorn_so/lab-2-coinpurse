package coinpurse;

/**
 * This interface is use for find a value of each object.
 * @author Pongsakorn Somsri
 *
 */
public interface Valuable extends Comparable<Valuable>{
	
	/**
	 * Method that use to get value.
	 * @return value of object
	 */
	public double getValue( );
	
}
