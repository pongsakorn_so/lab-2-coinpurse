package coinpurse;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * A MoneyFactory class the implement a factory pattern and singleton pattern it use to create a various type of
 * money up to the currency that was written in the purse.properties file.
 * @author Pongsakorn Somsri
 *
 */
public abstract class MoneyFactory {
	private static MoneyFactory moneyFactory = null;
	private static ResourceBundle bundle = null;

	/**
	 * A method that use to get a money factory object using a singleton pattern
	 * the factory that return up to factory that list in the purse.properties file.
	 * @return
	 */
	public static MoneyFactory getInstance() {
		if(bundle == null){
			try{
				bundle = ResourceBundle.getBundle("purse");
			}catch(Exception e){
				e.printStackTrace();
			}

		}

		if(moneyFactory == null){

			try {
				String factoryName = bundle.getString( "currency" );
				Object factory = Class.forName(factoryName).newInstance( );
				moneyFactory = (MoneyFactory)factory;
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				e.printStackTrace();
			}
		}



		return moneyFactory;
	}

	/**
	 * A method that use to create a money that equal the given value.
	 * @param value is the created money value.
	 * @return valuable object that equal the give value if program can't create that value it will throw illegalargument exception.
	 */
	public abstract Valuable createMoney(double value);

	/**
	 * Method use to create money from the given string value.
	 * @param value is the value of money that we want to create.
	 * @return money of the given value that created.
	 */
	public Valuable createMoney(String value){
		try{
			double moneyValue = Double.parseDouble(value);
			return createMoney(moneyValue);
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Method use for change the currency of the money.
	 * @param factory is the factory of what type of money we want to change to.
	 */
	public static void setMoneyFactory(MoneyFactory factory){
		moneyFactory = factory;
	}

}
